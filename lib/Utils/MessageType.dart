class MessageType {
  static const String ConnectionInvitation =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation";
  static const String ConnectionRequest =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/request";
  static const String ConnectionResponse =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/response";
  static const String TrustPingMessage =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/trust_ping/1.0/ping";
  static const String TrustPingResponseMessage =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/trust_ping/1.0/ping_response";
  static const String ForwardMessage =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/routing/1.0/forward";
  static const String Ack =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/notification/1.0/ack'";
  static const String ProposeCredential =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/propose-credential";
  static const String OfferCredential =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/offer-credential";
  static const String RequestCredential =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/request-credential";
  static const String IssueCredential =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/issue-credential";
  static const String CredentialAck =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/ack";
  static const String RequestPresentation =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/present-proof/1.0/request-presentation";
  static const String Presentation =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/present-proof/1.0/presentation";
  static const String PresentationAck =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/present-proof/1.0/ack";
  static const String BasicMessage =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/basicmessage/1.0/message";
  static const String CredentialPreview =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/issue-credential/1.0/credential-preview";

  // ADMIN Messages
  static const String AdminGetConnections =
      "https://github.com/hyperledger/aries-toolbox/tree/master/docs/admin-connections/0.1/get-list";
  static const String AdminListConnections =
      "https://github.com/hyperledger/aries-toolbox/tree/master/docs/admin-connections/0.1/list";
  static const String AdminCreateInvitation =
      "https://github.com/hyperledger/aries-toolbox/tree/master/docs/admin-invitations/0.1/create-invitation";
  static const String AdminListCredentials =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/admin-holder/0.1/credentials-get-list";
  static const String AdminAcceptCredential =
      "did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/admin-holder/0.1/credential-offer-accept";
  static const String AdminRecieveInvitation =
      "https://github.com/hyperledger/aries-toolbox/tree/master/docs/admin-connections/0.1/receive-invitation";
  static const String AdminConnectedConnection =
      "https://github.com/hyperledger/aries-toolbox/tree/master/docs/admin-connections/0.1/connection";
  static const String AdminDeleteConnection =
      "https://github.com/hyperledger/aries-toolbox/tree/master/docs/admin-connections/0.1/delete";
  static const String AdminDeletedConnection =
        "https://github.com/hyperledger/aries-toolbox/tree/master/docs/admin-connections/0.1/deleted";



}
