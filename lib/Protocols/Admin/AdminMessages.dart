import 'dart:convert';

import 'package:AriesFlutterMobileAgent/Utils/MessageType.dart';
import 'package:uuid/uuid.dart';

String adminConnections({
  bool responseRequested = true,
  String comment = '',
}) {
  Map<String, dynamic> trustedMessage = {
    '@id': Uuid().v4(),
    '~transport': {'return_route': 'all'},
    '@type': MessageType.AdminGetConnections,
    'comment': comment,
    'responseRequested': responseRequested
  };
  return jsonEncode(trustedMessage);
}

String adminDeleteConnection(String connectionId) {

  print('this is connectionId = ' + connectionId);

  Map<String, dynamic> trustedMessage = {
    "@type": MessageType.AdminDeleteConnection,
    '~transport': {'return_route': 'all'},
    "connection_id": connectionId
  };

  return jsonEncode(trustedMessage);
}

String adminCreateInvitation(label) {
  Map<String, dynamic> trustedMessage = {
    "@type": MessageType.AdminCreateInvitation,
    '~transport': {'return_route': 'all'},
    "alias": "Invitation to Liquid Avatar",
    "label": 'label',
    "group": "Liquid Avatar",
    "auto_accept": true,
    "multi_use": true,
    "mediation_id": "766754ba-2285-4f3b-9375-a10910fc5329"
  };

  return jsonEncode(trustedMessage);
}

String adminCredentials([offset = 0]) {
  Map<String, dynamic> trustedMessage = {
    "@type": MessageType.AdminListCredentials,
    '~transport': {'return_route': 'all'},
    "~paginate": {
      "limit": 10,
      "offset": 0
    }
  };

  return jsonEncode(trustedMessage);
}

String adminAcceptCredential(credentialId) {
  Map<String, dynamic> trustedMessage = {
    "@type": MessageType.AdminAcceptCredential,
    '~transport': {'return_route': 'all'},
    //@id': Uuid().v4(),
    "credential_exchange_id": credentialId
  };

  return jsonEncode(trustedMessage);
}

String adminAcceptInvitation(url) {

  Map<String, dynamic> trustedMessage = {
    "@type": MessageType.AdminRecieveInvitation,
    "invitation": url,
    "auto_accept": true,
    '~transport': {'return_route': 'all'},
  };

  return jsonEncode(trustedMessage);

}
